console.log('Beginning');
if (annyang) {
  // Let's define our first command. First the text we expect, and then the function it should call
  console.log('Annyang');
  var commands = {
    'hello': function() {
      alert('hello');
    },
	'scroll': function() {
		window.scrollBy(0, 100);
	}
  };

  annyang.addCallback('resultMatch', function(userSaid, commandText, phrases) {
	  console.log('User Said: ' + userSaid);
	  console.log('Command Text: ' + commandText);
	  console.log('phrases: ' + phrases);
  });
  // Add our commands to annyang
  annyang.addCommands(commands);
  console.log('Command added');

  // Start listening. You can call this here, or attach this call to an event, button, etc.
  annyang.start();
  console.log('Annyang started');
}

function create(htmlStr) {
    var frag = document.createDocumentFragment(),
        temp = document.createElement('div');
    temp.innerHTML = htmlStr;
    while (temp.firstChild) {
        frag.appendChild(temp.firstChild);
    }
    return frag;
}

var fragment = create('<div>Hello!</div><p>...</p>');
// You can use native DOM methods to insert the fragment:
document.body.insertBefore(fragment, document.body.childNodes[0]);