"use strict";var _typeof="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(a){return typeof a}:function(a){return a&&"function"==typeof Symbol&&a.constructor===Symbol&&a!==Symbol.prototype?"symbol":typeof a};
//! annyang
//! version : 2.6.0
//! author  : Tal Ater @TalAter
//! license : MIT
//! https://www.TalAter.com/annyang/
!function(a,b){"function"==typeof define&&define.amd?define([],function(){return a.annyang=b(a)}):"object"===("undefined"==typeof module?"undefined":_typeof(module))&&module.exports?module.exports=b(a):a.annyang=b(a)}("undefined"!=typeof window?window:void 0,function(a,b){var c,d=a.SpeechRecognition||a.webkitSpeechRecognition||a.mozSpeechRecognition||a.msSpeechRecognition||a.oSpeechRecognition;if(!d)return null;var e,f,g=[],h={start:[],error:[],end:[],soundstart:[],result:[],resultMatch:[],resultNoMatch:[],errorNetwork:[],errorPermissionBlocked:[],errorPermissionDenied:[]},i=0,j=0,k=!1,l="font-weight: bold; color: #00f;",m=!1,n=!1,o=/\s*\((.*?)\)\s*/g,p=/(\(\?:[^)]+\))\?/g,q=/(\(\?)?:\w+/g,r=/\*\w+/g,s=/[\-{}\[\]+?.,\\\^$|#]/g,t=function(a){return a=a.replace(s,"\\$&").replace(o,"(?:$1)?").replace(q,function(a,b){return b?a:"([^\\s]+)"}).replace(r,"(.*?)").replace(p,"\\s*$1?\\s*"),new RegExp("^"+a+"$","i")},u=function(a){for(var b=arguments.length,c=Array(b>1?b-1:0),d=1;d<b;d++)c[d-1]=arguments[d];a.forEach(function(a){a.callback.apply(a.context,c)})},v=function(){return e!==b},w=function(a,b){a.indexOf("%c")!==-1||b?console.log(a,b||l):console.log(a)},x=function(){v()||c.init({},!1)},y=function(a,b,c){g.push({command:a,callback:b,originalPhrase:c}),k&&w("Command successfully loaded: %c"+c,l)},z=function(a){u(h.result,a);for(var b,c=0;c<a.length;c++){b=a[c].trim(),k&&w("Speech recognized: %c"+b,l);for(var d=0,e=g.length;d<e;d++){var f=g[d],i=f.command.exec(b);if(i){var j=i.slice(1);return k&&(w("command matched: %c"+f.originalPhrase,l),j.length&&w("with parameters",j)),f.callback.apply(this,j),void u(h.resultMatch,b,f.originalPhrase,a)}}}u(h.resultNoMatch,a)};return c={init:function(l){var o=!(arguments.length>1&&arguments[1]!==b)||arguments[1];e&&e.abort&&e.abort(),e=new d,e.maxAlternatives=5,e.continuous="http:"===a.location.protocol,e.lang="en-US",e.onstart=function(){n=!0,u(h.start)},e.onsoundstart=function(){u(h.soundstart)},e.onerror=function(a){switch(u(h.error,a),a.error){case"network":u(h.errorNetwork,a);break;case"not-allowed":case"service-not-allowed":f=!1,(new Date).getTime()-i<200?u(h.errorPermissionBlocked,a):u(h.errorPermissionDenied,a)}},e.onend=function(){if(n=!1,u(h.end),f){var a=(new Date).getTime()-i;j+=1,j%10===0&&k&&w("Speech Recognition is repeatedly stopping and starting. See http://is.gd/annyang_restarts for tips."),a<1e3?setTimeout(function(){c.start({paused:m})},1e3-a):c.start({paused:m})}},e.onresult=function(a){if(m)return k&&w("Speech heard, but annyang is paused"),!1;for(var b=a.results[a.resultIndex],c=[],d=0;d<b.length;d++)c[d]=b[d].transcript;z(c)},o&&(g=[]),l.length&&this.addCommands(l)},start:function(a){x(),a=a||{},m=a.paused!==b&&!!a.paused,f=a.autoRestart===b||!!a.autoRestart,a.continuous!==b&&(e.continuous=!!a.continuous),i=(new Date).getTime();try{e.start()}catch(a){k&&w(a.message)}},abort:function(){f=!1,j=0,v()&&e.abort()},pause:function(){m=!0},resume:function(){c.start()},debug:function(){var a=!(arguments.length>0&&arguments[0]!==b)||arguments[0];k=!!a},setLanguage:function(a){x(),e.lang=a},addCommands:function(b){var c;x();for(var d in b)if(b.hasOwnProperty(d))if(c=a[b[d]]||b[d],"function"==typeof c)y(t(d),c,d);else{if(!("object"===("undefined"==typeof c?"undefined":_typeof(c))&&c.regexp instanceof RegExp)){k&&w("Can not register command: %c"+d,l);continue}y(new RegExp(c.regexp.source,"i"),c.callback,d)}},removeCommands:function(a){a===b?g=[]:(a=Array.isArray(a)?a:[a],g=g.filter(function(b){for(var c=0;c<a.length;c++)if(a[c]===b.originalPhrase)return!1;return!0}))},addCallback:function(c,d,e){var f=a[d]||d;"function"==typeof f&&h[c]!==b&&h[c].push({callback:f,context:e||this})},removeCallback:function(a,c){var d=function(a){return a.callback!==c};for(var e in h)h.hasOwnProperty(e)&&(a!==b&&a!==e||(c===b?h[e]=[]:h[e]=h[e].filter(d)))},isListening:function(){return n&&!m},getSpeechRecognizer:function(){return e},trigger:function(a){return c.isListening()?(Array.isArray(a)||(a=[a]),void z(a)):void(k&&w(n?"Speech heard, but annyang is paused":"Cannot trigger while annyang is aborted"))}}});

function levenshtein(a, b){
  if(a.length == 0) return b.length;
  if(b.length == 0) return a.length;

  var matrix = [];

  // increment along the first column of each row
  var i;
  for(i = 0; i <= b.length; i++){
    matrix[i] = [i];
  }

  // increment each column in the first row
  var j;
  for(j = 0; j <= a.length; j++){
    matrix[0][j] = j;
  }

  // Fill in the rest of the matrix
  for(i = 1; i <= b.length; i++){
    for(j = 1; j <= a.length; j++){
      if(b.charAt(i-1) == a.charAt(j-1)){
        matrix[i][j] = matrix[i-1][j-1];
      } else {
        matrix[i][j] = Math.min(matrix[i-1][j-1] + 1, // substitution
                                Math.min(matrix[i][j-1] + 1, // insertion
                                         matrix[i-1][j] + 1)); // deletion
      }
    }
  }
  return matrix[b.length][a.length];
}

function getSelectionParentElement() {
  var parentEl = null, sel;
  if (window.getSelection) {
    sel = window.getSelection();
    if (sel.rangeCount) {
      parentEl = sel.getRangeAt(0).commonAncestorContainer;
      if (parentEl.nodeType != 1) {
        parentEl = parentEl.parentNode;
      }
    }
  } else if ((sel = document.selection) && sel.type != "Control") {
    parentEl = sel.createRange().parentElement();
  }
  return parentEl;
}

function fuzzyFind (splat)  {
	var isFuzzy = splat.substring(0, 6) === "fuzzy ";
	if (isFuzzy) {
		splat = splat.substring(6);
	}
	var text = document.body.innerText;
	var len = splat.length;
	var minDistance = 999999999999;
	var bestMatch = "";
	var location = -1;
	var whiteSpacePatt = new RegExp("\\s");
	for (var i = 0; i < text.length - len; i++) {
		var substr = text.substring(i, i + len);
		if (whiteSpacePatt.test(substr.substring(i, i + 1))) {
			continue;
		}
		var editDistance = levenshtein(substr.toLowerCase(), splat.toLowerCase());
		if (editDistance < minDistance) {
			minDistance = editDistance;
			bestMatch = substr;
			location = i;
		}
	}
	if (!isFuzzy && minDistance !== 0) {
		alert('Exact match not found ' + ' try find fuzzy.');
	}
	else {
		window.find(bestMatch, false, false, true);
	}
	console.log("User Said: " + splat + " Best Match: " + bestMatch + " Distance: " + minDistance);
	
}

var currentReadNode = null;	
function readFromSelection(selection) {
  var parentElement = getSelectionParentElement();
  currentReadNode = parentElement;
  var msg = new SpeechSynthesisUtterance(parentElement.textContent);
  window.speechSynthesis.speak(msg);
}

console.log('Beginning');

if (annyang) {
  // Let's define our first command. First the text we expect, and then the function it should call
  console.log('Annyang');
  var links = document.querySelectorAll('a');
  var inputs = document.querySelectorAll('input[type]:not([type="hidden"])');
  var currentInput = -1;
  var commands = {
    'hello': function() {
      var msg = new SpeechSynthesisUtterance('Hi. How are you today?');
	  window.speechSynthesis.speak(msg);
    },
	'page down': function() {
		window.scrollBy(0, screen.height * .60);
	},
  'read title': function() {
    var msg = new SpeechSynthesisUtterance(document.title);
    window.speechSynthesis.speak(msg);
  },
  'read section': readFromSelection,
  'read next': function() {
	console.log(currentReadNode.nextElementSibling);
	if (currentReadNode === null) {
		return;
	}
	currentReadNode = currentReadNode.nextElementSibling;
	if (currentReadNode === null) {
		return;
	}
	var msg = new SpeechSynthesisUtterance(currentReadNode.textContent);
	window.speechSynthesis.speak(msg);
  },
	'page up': function() {
		window.scrollBy(0, screen.height * -.60);
	},
	'go back': function() {
		window.history.back();
	},
	'go forward': function() {
		window.history.forward();
	},
	'navigate *splat': function(splat) {
		var minDistance = 999999999999;
		var bestMatch = null;
		for (var i = 0; i < links.length; i++) {

			var item = links.item(i);
			var editDistance = levenshtein(item.innerHTML.toLowerCase(), splat.toLowerCase());

			if (editDistance < minDistance) {
				minDistance = editDistance;
				bestMatch = item;
			}
		}
		console.log("User Said: " + splat + " Best Match: "
					+ bestMatch.innerHTML.toLowerCase() + " Distance: " + minDistance);
		window.location.href = bestMatch.href;
	},
	'find *splat': fuzzyFind,
	'next input': function() {
		if (inputs.length === 0) {
			return;
		}
		currentInput++;
		console.log(currentInput);
		console.log(inputs.length);
		if (!(currentInput < inputs.length)) {
			currentInput = 0;
		}
		console.log(inputs[0]);
		inputs[0].focus();
	},
	'type *splat': function(splat) {
		document.activeElement.value = splat;
	},
	'submit form': function() {
		var form = document.querySelector('form');
		if (form !== null) {
			form.submit();
		}
	}
  };

  annyang.addCallback('resultMatch', function(userSaid, commandText, phrases) {
	  console.log('User Said: ' + userSaid);
	  console.log('Command Text: ' + commandText);
	  console.log('phrases: ' + phrases);
  });

  annyang.addCallback('resultNoMatch', function(userSaid) {
	  console.log('User Said: ' + userSaid);
  });
  // Add our commands to annyang
  annyang.addCommands(commands);
  console.log('Command added');

  // Start listening. You can call this here, or attach this call to an event, button, etc.
  annyang.start();
  console.log('Annyang started');

}
